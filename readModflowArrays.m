function H = readModflowArrays(filename)
%http://gwlab.blogspot.gr/2012/08/reading-modflow-output-files-again.html

fid = fopen(filename, 'r');
k = 0;
while ~feof(fid)
    k = k + 1;
    H(k,1).KSPT = fread(fid, 1, 'uint'); 
    H(k,1).KPER = fread(fid, 1, 'uint');
    H(k,1).PERTIM = fread(fid, 1, 'float');
    H(k,1).TOTIM = fread(fid, 1, 'float');
    H(k,1).DESC = fread(fid, 16, 'char');
    H(k,1).DESC = char(H(k,1).DESC');
    H(k,1).NCOL = fread(fid, 1, 'uint');
    H(k,1).NROW = fread(fid, 1, 'uint');
    H(k,1).ILAY = fread(fid, 1, 'uint');
    fprintf('Reading %s for time step %i, stress period %i\n',...
        H(k,1).DESC, H(k,1).KSPT, H(k,1).KPER);
    H(k,1).data = zeros(H(k,1).NROW, H(k,1).NCOL);
    for ir = 1 : H(k,1).NROW
        for ic = 1 : H(k,1).NCOL
            H(k,1).data(ir,ic)=fread(fid, 1, 'float');
        end
    end
end


fclose(fid);
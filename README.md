# Modflow Utilities #

This repository contains functions for reading the Modflow binary output files. Currently the code is written in matlab. 

### How do I get set up? ###
Get the code and put it in a folder that matlab can see

### How to use it? ###
Check the [wiki](https://bitbucket.org/giorgk/modflow_utilities/wiki/Home)

### Can I help? ###
**YES**. The greatest need is to find examples that cover as many output types as possible. If your file cannot be read then it is a good candidate example. 

### Who do I talk to? ###

* Repo owner -> gkourakos@ucdavis.edu